// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

extern "C" {
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <ply-boot-client.h>
#include <ply-event-loop.h>
#include <vmwgfx_drm.h>
#include <xf86drm.h>
}

#include <filesystem>
#include <iostream>

#include <KLocalizedString>

struct FileDescriptor {
    FileDescriptor(int fd)
        : m_fd(fd)
    {
    }
    ~FileDescriptor()
    {
        close(m_fd);
    }
    Q_DISABLE_COPY_MOVE(FileDescriptor)
    int m_fd;
};

struct PlyClient {
    PlyClient()
        : m_client(ply_boot_client_new())
    {
    }
    ~PlyClient()
    {
        ply_boot_client_flush(m_client);
        ply_boot_client_disconnect(m_client);
        ply_boot_client_free(m_client);
    }
    Q_DISABLE_COPY_MOVE(PlyClient)
    ply_boot_client_t *m_client;
};

struct PlyLoop {
    PlyLoop()
        : m_loop(ply_event_loop_new())
    {
    }
    ~PlyLoop()
    {
        ply_event_loop_free(m_loop);
    }
    Q_DISABLE_COPY_MOVE(PlyLoop)
    ply_event_loop_t *m_loop;
};

namespace
{
void on_keystroke(void *user_data, const char *answer, ply_boot_client_t *client)
{
    auto loop = (PlyLoop *)user_data;
    ply_event_loop_exit(loop->m_loop, 0);
}
}

int main(int argc, char **argv)
{
    if (!std::filesystem::exists("/sys/module/vmwgfx")) {
        return 0;
    }

    FileDescriptor fd(open("/dev/dri/card0", O_RDONLY));

    struct drm_vmw_getparam_arg gp_arg {
    };
    gp_arg.param = DRM_VMW_PARAM_3D;
    const auto ret = drmCommandWriteRead(fd.m_fd, DRM_VMW_GET_PARAM, &gp_arg, sizeof(gp_arg));
    if (ret == 0 && gp_arg.value != 0) {
        return 0; // 3D enabled
    }

    std::cerr << "No 3D acceleration\n";

    PlyClient client;
    std::cout << "connect\n";
    if (!ply_boot_client_connect(client.m_client, nullptr, nullptr)) {
        std::cout << "No plymouth running\n";
        return 0;
    }

    PlyLoop loop;
    ply_boot_client_attach_to_event_loop(client.m_client, loop.m_loop);

    const auto line1 = i18nc("warning message line 1", "Oopsie whoopsie you have no 3D acceleration. I am vevvy disappointed UwU.");
    const auto line2 = i18nc("warning message line 2. Y is hardcoded, do not localized it", "keys:Press Y to continue anyway");
    for (const auto &line : {line1, line2}) {
        ply_boot_client_tell_daemon_to_display_message(client.m_client, qUtf8Printable(line), nullptr, nullptr, nullptr);
    }

    ply_boot_client_ask_daemon_to_watch_for_keystroke(client.m_client, "yY", on_keystroke, nullptr, &loop);

    // When we get here the user typed Y. Otherwise we wait forever.
    return ply_event_loop_run(loop.m_loop);
}
